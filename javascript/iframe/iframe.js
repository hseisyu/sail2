// SAGE2 is available for use under the SAGE2 Software License
//
// University of Illinois at Chicago's Electronic Visualization Laboratory (EVL)
// and University of Hawai'i at Manoa's Laboratory for Advanced Visualization and
// Applications (LAVA)
//
// See full text, terms and conditions in the LICENSE.txt included file
//
// Copyright (c) 2015



var iframe = SAGE2_App.extend( {
	init: function(data) {
		this.SAGE2Init("iframe", data);

		this.resizeEvents = "continuous"; //"onfinish";
		this.element.id = "div" + data.id;

		// Get width height from the supporting div		
		var width  = this.element.clientWidth;
		var height = this.element.clientHeight;
		this.element.style.width  = width  + "px"; 
		this.element.style.height = height + "px"; 

		this.element.frameborder = 0;
		this.element.style.backgroundColor = "white";

		var url = "http://viewstl.com/";
		this.element.setAttribute("src", url); 
	},

	load: function(date) {
	},

	draw: function(date) {
	},

	resize: function(date) {
		var width  = this.element.clientWidth;
		var height = this.element.clientHeight;
		console.log('resize', this.element.clientWidth, this.element.clientHeight);
		this.element.style.width  = width  + "px"; 
		this.element.style.height = height + "px";
		this.refresh(date);
	},
	
	event: function(eventType, position, user_id, data, date) {
		if (eventType === "pointerPress" && (data.button === "left") ) {
		}
		if (eventType === "pointerMove" ) {

		}
		if (eventType === "pointerRelease" && (data.button === "left") ) {
		}
	}
	
});

