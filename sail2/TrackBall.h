/*
** 簡易トラックボール処理
*/

namespace TrackBall {

extern void init(void);
extern void region(int w, int h);
extern void start(int x, int y);
extern void motion(int x, int y);
extern void stop(int x, int y);
extern double *rotation(void);

}
