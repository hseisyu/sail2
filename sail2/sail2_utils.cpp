// SAGE2 is available for use under the SAGE2 Software License
//
// University of Illinois at Chicago's Electronic Visualization Laboratory (EVL)
// and University of Hawai'i at Manoa's Laboratory for Advanced Visualization and
// Applications (LAVA)
//
// See full text, terms and conditions in the LICENSE.txt included file
//
// Copyright (c) 2014

#include <sys/time.h>

/**
 * @module sail2_utils
 * @class  sail2_utils
 */


struct timeval tv_start;

/**
 * Initialize a timer at initialization
 *
 * @method initTime
 */

void initTime()
{
    gettimeofday(&tv_start, 0);
}

/**
 * Returns the amount of time elspased since start of the program
 *
 * @method getTime
 * @return {double} time in micro second
 */

double getTime()
{
    struct timeval tv;
    gettimeofday(&tv, 0);
    return (double)(tv.tv_sec  - tv_start.tv_sec)*1000000.0 + 
	   (double)(tv.tv_usec - tv_start.tv_usec);
}

