// SAGE2 is available for use under the SAGE2 Software License
//
// University of Illinois at Chicago's Electronic Visualization Laboratory (EVL)
// and University of Hawai'i at Manoa's Laboratory for Advanced Visualization and
// Applications (LAVA)
//
// See full text, terms and conditions in the LICENSE.txt included file
//
// Copyright (c) 2014


#ifndef SAIL2_H
#define SAIL2_H

#include <stdio.h>
#include <stdlib.h>
#include "websocketio.h"
#include "sail2_utils.h"

// enum sagePixFmt {PIXFMT_NULL, PIXFMT_555, PIXFMT_555_INV, PIXFMT_565, PIXFMT_565_INV, 
//       PIXFMT_888, PIXFMT_888_INV, PIXFMT_8888, PIXFMT_8888_INV, PIXFMT_RLE, PIXFMT_LUV,
//       PIXFMT_DXT, PIXFMT_YUV, PIXFMT_RGBS3D, PIXFMT_DXT5YCOCG};

enum sagePixFmt {
		PIXFMT_NULL,	// 0
		PIXFMT_888,		// RGB 24bit
		PIXFMT_YUV		// YUV422 packed
};

class sail {

private:
	int   width, height;
	float frameRate;
	enum  sagePixFmt format;
	unsigned char *frame;

public:
	sail(const char *appname, int ww, int hh, enum sagePixFmt pixelfmt, std::string hostname, float frate);
	~sail();

	int getWidth()  { return width;};
	int getHeight() { return height;};

	// Return the frame size in bytes (based on width, height and pixel type)
	int getBufferSize();

	// Returns a pointer to the current frame buffer
	unsigned char* getBuffer() {return frame;};

	WebSocketIO* wsio;
	std::string uniqueID;
	std::string ws_host;
	std::string applicationName;

	bool got_request;
	bool got_frame;
};


	// Create and initialize a SAIL object
sail* createSAIL(const char *appname, int ww, int hh, enum sagePixFmt pixelfmt,
	const char *url, float frate = 30.0);

	// Disconnect and delete a SAIL object
void  deleteSAIL(sail *sageInf);

	// Return the next buffer to be filled
std::string nextBuffer(sail *sageInf);

	// Swap buffers and stream
void swapBuffer(sail *sageInf);

	// Fill the next buffer and swap
void swapWithBuffer(sail *sageInf, unsigned char *pixptr);

	// Process messages from SAGE2 server
void processMessages(sail *sageInf);



#endif // SAIL2_H

