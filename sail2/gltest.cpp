#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#if defined(WIN32)
//#  pragma comment(linker, "/subsystem:\"windows\" /entry:\"mainCRTStartup\"")
#  include "glut.h"
#  include "glext.h"
#elif defined(__APPLE__) || defined(MACOSX)
#  include <GLUT/glut.h>
#else
#  define GL_GLEXT_PROTOTYPES
#  include <GL/glut.h>
#endif

#ifndef M_PI
#define M_PI 3.1415926536
#endif

#include "sail2.h"

sail *sageInf;

/* 回転角 */
static double angle;
static bool isMoving = true;

/*
 ** 光源、物質（とそのデフォルト値）
 */
double r = 5;
double v = 10;
int mtype = 0;
float shininess = 100.0f;

GLfloat light[3][4]; /* Ambient, Diffuse, Specular */

const GLfloat light_d[3][4] =
{
	{ 0.3, 0.3, 0.3, 1.0 },
	{ 1.0, 1.0, 1.0, 1.0 },
	{ 1.0, 1.0, 1.0, 1.0 }
};

char mstr[][256] =
{"Default", "Ambient only", "Diffuse only", "Specular only",
 "Ambient and Diffuse", "Diffuse and Specular", "Ambient and Specular", "Other Sample"};

GLfloat material[3][4]; /* Ambient, Diffuse, Specular */

const GLfloat m_all[3][4] =
{
	{ 0.8, 0.3, 0.3, 1.0 },
	{ 0.8, 0.3, 0.3, 1.0 },
	{ 1.0, 1.0, 1.0, 1.0 },
};

const GLfloat m_ambient[3][4] =
{
	{ 0.8, 0.3, 0.3, 1.0 },
	{ 0.0, 0.0, 0.0, 0.0 },
	{ 0.0, 0.0, 0.0, 0.0 },
};

const GLfloat m_diffuse[3][4] =
{
	{ 0.0, 0.0, 0.0, 0.0 },
	{ 0.8, 0.3, 0.3, 1.0 },
	{ 0.0, 0.0, 0.0, 0.0 },
};

const GLfloat m_specular[3][4] =
{
	{ 0.0, 0.0, 0.0, 0.0 },
	{ 0.0, 0.0, 0.0, 0.0 },
	{ 1.0, 1.0, 1.0, 1.0 },
};

const GLfloat m_amb_diff[3][4] =
{
	{ 0.8, 0.3, 0.3, 1.0 },
	{ 0.8, 0.3, 0.3, 1.0 },
	{ 0.0, 0.0, 0.0, 0.0 },
};

const GLfloat m_diff_spec[3][4] =
{
	{ 0.0, 0.0, 0.0, 0.0 },
	{ 0.8, 0.3, 0.3, 1.0 },
	{ 1.0, 1.0, 1.0, 1.0 },
};

const GLfloat m_amb_spec[3][4] =
{
	{ 0.8, 0.3, 0.3, 1.0 },
	{ 0.0, 0.0, 0.0, 0.0 },
	{ 1.0, 1.0, 1.0, 1.0 },
};

const GLfloat m_emerald[3][4] = /* shininess : 76.8 */
{
	{ 0.0215, 0.1745, 0.0215, 1.0 },
	{ 0.07568, 0.61424, .07568, 1.0 },
	{ 0.633, 0.727811, 0.633, 1.0 }
};

void cpProp(GLfloat (*dest)[4], GLfloat (*src)[4])
{
	for(int i = 0; i < 3; i++)
		for(int j = 0; j < 4; j++)
			dest[i][j] = src[i][j];
	return;
}

void updateLight()
{
	glLightfv(GL_LIGHT0, GL_AMBIENT, light[0]);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, light[1]);
	glLightfv(GL_LIGHT0, GL_SPECULAR, light[2]);

	/* 光源の位置を設定 */
	const double x = r * cos(angle * M_PI / 180);
	const double y = r * sin(angle * M_PI / 180);

	const GLfloat lightpos[] = { x, y, 0.0, 1.0 };
	glTranslated(x, y, 0.0);

	glDisable(GL_LIGHTING);
	glColor3f(1.0, 0.5, 0.5);

	glutSolidSphere(0.1, 30, 30);
	glEnable(GL_LIGHTING);

	glLightfv(GL_LIGHT0, GL_POSITION, lightpos);

}

/*
 ** 初期化
 */
static void init(void) {
	/* 初期設定 */
	glEnable(GL_DEPTH_TEST);
	glDisable(GL_CULL_FACE);

	cpProp(light, (GLfloat (*)[4])light_d);
	cpProp(material, (GLfloat (*)[4])m_all);

	/* 光源の初期設定 */
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
	glLightModeli(GL_LIGHT_MODEL_LOCAL_VIEWER, GL_TRUE);

}

static void drawTexts(int x, int y, char* str) {
	glPushAttrib(GL_CURRENT_BIT | GL_ENABLE_BIT);

	glDisable(GL_LIGHTING);
	glDisable(GL_TEXTURE_2D);

	// Get viewport information.
	GLint vp[4];
	glGetIntegerv(GL_VIEWPORT, vp);
	const GLint left = vp[0];
	const GLint bottom = vp[1];
	const GLint right = vp[2];
	const GLint top = vp[3];

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	{
		glLoadIdentity();

		const float front = 0.0f;
		const float back = 2000.0f;
		glOrtho(left, right, bottom, top, front, back);

		glMatrixMode(GL_MODELVIEW);
		glPushMatrix();
		{
			glLoadIdentity();
			glColor3ub(1.0, 1.0, 1.0);

			for (size_t line = 0; line < 20; line++) {
				glRasterPos2i(x, y);

				char* line_head = const_cast<char*> (str);
				for (char* p = line_head; *p; p++) {
					glutBitmapCharacter(GLUT_BITMAP_8_BY_13, *p);
				}
			}
		}
		glPopMatrix();
		glMatrixMode(GL_PROJECTION);
	}
	glPopMatrix();
	glMatrixMode(GL_MODELVIEW);

	glPopAttrib();
}

/*
 ** シーンの描画
 */
static void scene(void) {

	glBegin(GL_LINE_LOOP);
	for (double d = 0; d < 360; d += 5) {
		const double x = r * cos(d * M_PI / 180);
		const double y = r * sin(d * M_PI / 180);
		glVertex3d(x, y, 0.0);
	}
	glEnd();

	/* 材質の設定 */
	glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, material[0]);
	glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, material[1]);
	glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, material[2]);
	glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, shininess);

	glutSolidTeapot(1.0);

	updateLight();

	char str[256];

	sprintf(str, "Material Type: %s ('c' key to change)", mstr[mtype]);
	drawTexts(15, 30, str);

	sprintf(str, "Shininess: 'w' <- %.3f -> 's' (For only specular element)", shininess);
	drawTexts(15, 15, str);

}

/****************************
 ** GLUT のコールバック関数 **
 ****************************/

/* トラックボール処理用関数の宣言 */
#include "TrackBall.h"

static void display(void) {
	/* モデルビュー変換行列の初期化 */
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	/* 画面クリア */
	glClearColor(1.0, 1.0, 1.0, 0.0);
	//glClearColor(0.9 + lightamb[0] / 10, 0.9 + lightamb[1] / 10, 0.9 + lightamb[2] / 10, 0.0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	/* 視点の移動（物体の方を奥に移動）*/
	glTranslated(0.0, 0.0, -10.0);

	/* トラックボール処理による回転 */
	glMultMatrixd(TrackBall::rotation());

	/* シーンの描画 */
	scene();

	/* ダブルバッファリング */
	glutSwapBuffers();

	///////////////////////////////

	int imageWidth = 640;
	int imageHeight = 480;

	void* dataBuffer = NULL;
	int size = imageWidth * imageHeight * 3;
	dataBuffer = malloc( size );

	glReadBuffer( GL_BACK );
	glReadPixels(
		0,                 //ｿｿｿｿｿｿｿｿｿｿｿxｿｿ
		0,                 //ｿｿｿｿｿｿｿｿｿｿｿyｿｿ //0 or getCurrentWidth() - 1
		imageWidth,             //ｿｿｿｿｿｿｿｿ
		imageHeight,            //ｿｿｿｿｿｿｿｿｿ
		GL_BGR, //it means GL_BGR,           //ｿｿｿｿｿｿｿｿｿｿｿ
		GL_UNSIGNED_BYTE,  //ｿｿｿｿｿｿｿｿｿｿｿｿｿｿｿｿｿ
		( GLubyte* )dataBuffer      //ｿｿｿｿｿｿｿｿｿｿｿｿｿｿｿｿｿｿｿｿｿｿｿｿｿｿｿｿｿｿｿ
		);

/*
	FILE *fp;
	fp = fopen("test.rgb", "wb");
	if(fp != NULL) {
		fwrite(dataBuffer, size, 1, fp);
	}
*/

	fprintf(stderr, "loop\n");
	swapWithBuffer(sageInf, (unsigned char*)dataBuffer);
	processMessages(sageInf);

	free(dataBuffer);
}

static void resize(int w, int h) {
	/* トラックボールする範囲 */
	TrackBall::region(w, h);

	/* ウィンドウ全体をビューポートにする */
	glViewport(0, 0, w, h);

	/* 透視変換行列の指定 */
	glMatrixMode(GL_PROJECTION);

	/* 透視変換行列の初期化 */
	glLoadIdentity();

	gluPerspective(60.0, (double) w / (double) h, 1.0, 100.0);

}

static void idle(int d) {
	if(isMoving) {
		angle += v;
		if (angle >= 360.0)
			angle = 0.0;
	}

	/* 画面の描き替え */
	glutPostRedisplay();
	glutTimerFunc(150, idle, 0);
}

static void mouse(int button, int state, int x, int y) {
	switch (button) {
	case GLUT_LEFT_BUTTON:
		switch (state) {
		case GLUT_DOWN:
			/* トラックボール開始 */
			TrackBall::start(x, y);
			break;
		case GLUT_UP:
			/* トラックボール停止 */
			TrackBall::stop(x, y);
			break;
		default:
			break;
		}
		break;
	default:
		break;
	}
}

static void motion(int x, int y) {
	/* トラックボール移動 */
	TrackBall::motion(x, y);
}

static void keyboard(unsigned char key, int x, int y) {
	switch (key) {
	case 'w': shininess += 10; break;
	case 's': shininess -= 10; break;
	case 'v': v *= 2; break;
	case 'b': v /= 2; break;
	case 'c':
		mtype = (mtype + 1) % 8;
		switch(mtype) {
			case 0: cpProp(material, (GLfloat (*)[4])m_all); break;
			case 1: cpProp(material, (GLfloat (*)[4])m_ambient); break;
			case 2: cpProp(material, (GLfloat (*)[4])m_diffuse); break;
			case 3: cpProp(material, (GLfloat (*)[4])m_specular); break;
			case 4: cpProp(material, (GLfloat (*)[4])m_amb_diff); break;
			case 5: cpProp(material, (GLfloat (*)[4])m_diff_spec); break;
			case 6: cpProp(material, (GLfloat (*)[4])m_amb_spec); break;
			case 7: cpProp(material, (GLfloat (*)[4])m_emerald); break;
		}
		break;
	case 'm':
		isMoving = !isMoving;
		break;
	//case 'w': lightamb[]; break;
	case 'q':
	case 'Q':
	case '\033':
		/* ESC か q か Q をタイプしたら終了 */
		exit(0);
	default:
		break;
	}
}


int sail_init(char *argv[])
{
	fprintf(stderr, "Main: %s\n", argv[1]);

	// Create SAIL2 object
	sageInf = createSAIL("testapp", 640, 480, PIXFMT_888, argv[1], 30);
	fprintf(stderr, "App created\n");

/*

        int  count = 0;
        bool done  = false;

        double t1, t2, fps, interval, rate;
        rate = 5.0;
        interval = 1000000.0 / rate;
*/

}

/*
 ** メインプログラム
 */
int main(int argc, char *argv[]) {
	sail_init(argv);

	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGBA | GLUT_DEPTH | GLUT_DOUBLE);
	glutInitWindowSize(600, 600);
	glutCreateWindow(argv[0]);
	glutDisplayFunc(display);
	glutReshapeFunc(resize);
	glutTimerFunc(1, idle, 0);
	glutMouseFunc(mouse);
	glutMotionFunc(motion);
	glutKeyboardFunc(keyboard);
	init();
	glutMainLoop();
	return 0;
}
