# JPEG module needs libjpeg and a compiler
  * using a forked version so it compiles with modern nodejs versions

# VNC module rfb2
  * modified to provide a frame callback
  
# To run

node vnc_client.js -h

  Usage: vnc_client [options]

  Options:

    -h, --help                 output usage information
    -V, --version              output the version number
    -s, --server <value>       server hostname (127.0.0.1)
    -p, --port <n>             port number (5900)
    -c, --catchphrase <value>  password (secret)
    -d, --display <value>      SAGE url (wss://localhost:443)


Example: node vnc_client.js -s galette -p 5901 -c '#####' -d wss://localhost:443

